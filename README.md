# Consulta IPTU Data Service

[![pipeline status](https://gitlab.com/villalabs/pucminas/arquitetura-software-distribuido/integracao/consulta-iptu/badges/master/pipeline.svg)](https://gitlab.com/villalabs/pucminas/arquitetura-software-distribuido/integracao/consulta-iptu/-/commits/master)

~~~sh
# Adiciona Maven no PATH
$ export PATH=$PATH:/c/apache-maven-3.6.3/bin
# Adiciona JAVA_HOME
$ export JAVA_HOME=/c/clientes/CNPq/dev/jdk/jdk1.8.0_201

# Buildando projeto
$ mvn clean install -Dmaven.test.skip=true

# Copia para container
$ export POD_MI=$(kubectl get pod -n wso2-tools --selector="app.kubernetes.io/instance=wso2-micro-integrator,app.kubernetes.io/name=wso2-micro-integrator" --output jsonpath='{.items[0].metadata.name}')
$ kubectl cp car/target/consulta-iptu_1.0.0.car wso2-tools/${POD_MI}:/home/wso2carbon/wso2mi-1.2.0/repository/deployment/server/carbonapps
~~~